# Rust color scheme for CLion and IntelliJ IDEA

A file containing a custom color scheme for Rust development that may be imported into CLion or IntelliJ IDEA.

Also includes an image file that may be used as the background image for the IDE.

The scheme is dark for a dark theme and similair to that of Visual Studio Code.
